Document: coveralls
Title: Debian coveralls Manual
Author: <insert document author here>
Abstract: This manual describes what coveralls is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/coveralls/coveralls.sgml.gz

Format: postscript
Files: /usr/share/doc/coveralls/coveralls.ps.gz

Format: text
Files: /usr/share/doc/coveralls/coveralls.text.gz

Format: HTML
Index: /usr/share/doc/coveralls/html/index.html
Files: /usr/share/doc/coveralls/html/*.html
