Document: python3-pyfavicon
Title: Debian python3-pyfavicon Manual
Author: <insert document author here>
Abstract: This manual describes what python3-pyfavicon is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/python3-pyfavicon/python3-pyfavicon.sgml.gz

Format: postscript
Files: /usr/share/doc/python3-pyfavicon/python3-pyfavicon.ps.gz

Format: text
Files: /usr/share/doc/python3-pyfavicon/python3-pyfavicon.text.gz

Format: HTML
Index: /usr/share/doc/python3-pyfavicon/html/index.html
Files: /usr/share/doc/python3-pyfavicon/html/*.html
