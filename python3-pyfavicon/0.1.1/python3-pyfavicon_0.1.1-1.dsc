Format: 3.0 (quilt)
Source: python3-pyfavicon
Binary: python3-python3-pyfavicon, python-python3-pyfavicon-doc
Architecture: all
Version: 0.1.1-1
Maintainer: debian <anjan@momi.ca>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), dh-python, python3-setuptools, python3-all, python3-aiohttp, python3-pytest-cov, python3-coveralls
Package-List:
 python-python3-pyfavicon-doc deb doc optional arch=all
 python3-python3-pyfavicon deb unknown optional arch=all
Checksums-Sha1:
 dd459baf7479f5da2e1ddfe97068bd8cfe450de0 9498 python3-pyfavicon_0.1.1.orig.tar.gz
 b46255b2bde028e482e844176ea2e3d6ed188886 8644 python3-pyfavicon_0.1.1-1.debian.tar.xz
Checksums-Sha256:
 7bd832c3b131a13789492909d5000dc3993ad54939615577f1545c53aa10f88f 9498 python3-pyfavicon_0.1.1.orig.tar.gz
 0b637a4a4f11a22853a6e19597c140b0f3feb118dcf5d704076e6a53c747b183 8644 python3-pyfavicon_0.1.1-1.debian.tar.xz
Files:
 260168d2b7fc0f90baa487f93d1dc46c 9498 python3-pyfavicon_0.1.1.orig.tar.gz
 2564aa703f8ad0f33b38ba9201c9d702 8644 python3-pyfavicon_0.1.1-1.debian.tar.xz
