��    
      l      �       �      �      �             !   2     T     i     }     �  �  �     7     ?     S  ,   k  &   �     �     �     �     �                         
   	                 Add Add a new account Authenticator Authenticator version number Couldn't generate the secret code Lock the application Start in debug mode Undo Unlock Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-11-03 21:23+0000
Last-Translator: Jack Fredericksen <jack.a.fredericksen@gmail.com>
Language-Team: Danish <https://hosted.weblate.org/projects/authenticator/translation/da/>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 3.3-dev
 Tilføj Tilføj en ny konto TofaktorAutentifikation Gnome-TofaktorAutentifikation versionsnummer Kunne ikke generere den hemmelige kode Lås applikationen Start i debug-tilstand Fortryd Lås op 