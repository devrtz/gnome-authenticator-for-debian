Format: 3.0 (quilt)
Source: coveralls
Binary: python3-coveralls, python-coveralls-doc
Architecture: all
Version: 2.0.0-1
Maintainer: debian <anjan@momi.ca>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), dh-python, python3-setuptools, python3-all
Package-List:
 python-coveralls-doc deb doc optional arch=all
 python3-coveralls deb unknown optional arch=all
Checksums-Sha1:
 39487ac5932c6d9533ef08e41a66fdb24bdbb06d 14828 coveralls_2.0.0.orig.tar.gz
 aa2617e16c85b881465f5dd133fc0451586a1d0d 8016 coveralls_2.0.0-1.debian.tar.xz
Checksums-Sha256:
 d213f5edd49053d03f0db316ccabfe17725f2758147afc9a37eaca9d8e8602b5 14828 coveralls_2.0.0.orig.tar.gz
 5c0122853cbd4262845a60093b945032fe96bec9691a13b6602ad7af795e6dd2 8016 coveralls_2.0.0-1.debian.tar.xz
Files:
 1895bb61f252ba5543dad03b6503baa5 14828 coveralls_2.0.0.orig.tar.gz
 87c0e7c8c5f7caf9ee777c24b33d55fb 8016 coveralls_2.0.0-1.debian.tar.xz
