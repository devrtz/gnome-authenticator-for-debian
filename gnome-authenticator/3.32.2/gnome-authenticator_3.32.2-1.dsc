Format: 3.0 (quilt)
Source: gnome-authenticator
Binary: gnome-authenticator
Architecture: any
Version: 3.32.2-1
Maintainer: <anjan@momi.ca>
Homepage: https://gitlab.gnome.org/World/Authenticator
Standards-Version: 4.5.0
Vcs-Browser: https://gitlab.gnome.org/World/Authenticator
Vcs-Git: https://gitlab.gnome.org/World/Authenticator.git
Build-Depends: debhelper-compat (= 12), meson, libglib2.0-dev, libgirepository1.0-dev, cmake, libgtk-3-dev, gobject-introspection
Package-List:
 gnome-authenticator deb web optional arch=any
Checksums-Sha1:
 85999ff1df1c4e017b98746f31e0f96ba494aa84 664616 gnome-authenticator_3.32.2.orig.tar.gz
 9d61afe47705bb7157f30809a9fb0f73db97d7c1 7992 gnome-authenticator_3.32.2-1.debian.tar.xz
Checksums-Sha256:
 d7250fbef1de2dcb599a55ff3d40dcc6ed5618fe89d50ec795281d8ba3123d5b 664616 gnome-authenticator_3.32.2.orig.tar.gz
 986ace2f1e3dabb8d02902638d76c8673d5a82b60ebf75a2526c53d76667e899 7992 gnome-authenticator_3.32.2-1.debian.tar.xz
Files:
 5ca5f6ee8abb0ded676cee94179ae42d 664616 gnome-authenticator_3.32.2.orig.tar.gz
 a04a38606fe893dfb54d5e81819c7477 7992 gnome-authenticator_3.32.2-1.debian.tar.xz
